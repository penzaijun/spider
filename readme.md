config.py中修改账号\密码\重复生成次数\输入json路径\进程数量

执行spider_multiprocess.py，运行过程中不要最小化浏览器界面

由于openai网站会要求单个账户只能有一个正在询问的问题，所以请保证自己的用户列表和别人没有重合

输入json格式:

    input=[[dialog0_sentence0,dialog0_sentence1,...],[dialog1_sentence0,dialog1_sentence1,...],..]

    输入json为一个数组，其每个元素都为一个对话。对话以str数组形式存储，每个str为向chatgpt提的一个问题，按先后顺序依次输入。 

输出文件格式:

    output={idx:dialog_idx,response:[[sentence0_response0,sentence0_response1,...],[sentence1_response0,sentence1_response1,...],...]}\n...
    
    输出json为一个文本文件，其每行都为一个对话的回复，用\n分割。对话的回复以字典形式存储，key'idx'对应该对话在输入中的顺序，key'response'对应该对话的回复内容

目前不能处理的情况:
1. Access denied (使用国内ip访问chatgpt) 
2. 服务器满占用情况
3. chatgpt网页中，重新生成按钮不存在的情况     √
4. 人机验证失败的情况(概率很低）               √
5. 网络延迟极大或者生成时间极长的情况