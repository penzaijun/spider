from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import undetected_chromedriver as uc
import time
import os
import json
from config import * 
import logging
import random

if not os.path.exists('./output'): os.mkdir('./output')

ACCOUNT_RESTRICTED='account restricted'
ACCOUNT_USING='account using'


def get_sentence_result(driver,index,regenerate_time=REGENERATE_COUNT) -> list[str]:
    time.sleep(3)
    try:
        driver.find_element(By.XPATH,"//div[@class='py-2 px-3 border text-gray-600 rounded-md text-sm dark:text-gray-100 border-red-500 bg-red-500/10']")
        return ACCOUNT_RESTRICTED
    except Exception as e:
        pass
    WebDriverWait(driver,30,1).until(ec.presence_of_element_located((By.XPATH,"//button[@class='btn relative btn-neutral border-0 md:border']")))
    time.sleep(1)
    WebDriverWait(driver,180,1).until_not(ec.text_to_be_present_in_element((By.XPATH,"//button[@class='btn relative btn-neutral border-0 md:border']"),'Stop generating'))
    try:
        driver.find_element(By.XPATH,"//div[@class='py-2 px-3 border text-gray-600 rounded-md text-sm dark:text-gray-100 border-red-500 bg-red-500/10']")
        return ACCOUNT_RESTRICTED
    except Exception as e:
        pass
    sentence_responses=[]
    sentence_responses.append(driver.find_elements(By.XPATH,"//div[@class='markdown prose w-full break-words dark:prose-invert light']")[index].text)
    time.sleep(3)
    for _ in range(regenerate_time):
        try:
            driver.find_element(By.XPATH,"//button[@class='btn flex justify-center gap-2 btn-neutral border-0 md:border']").click()
        except Exception as e:
            logging.error('regenerate button not found, give up regenerating')
            logging.error(str(e))
            return sentence_responses
        WebDriverWait(driver,30,1).until(ec.presence_of_element_located((By.XPATH,"//button[@class='btn flex justify-center gap-2 btn-neutral border-0 md:border']")))
        time.sleep(1)
        WebDriverWait(driver,180,1).until_not(ec.text_to_be_present_in_element((By.XPATH,"//button[@class='btn flex justify-center gap-2 btn-neutral border-0 md:border']"),'Stop generating'))
        try:
            driver.find_element(By.XPATH,"//div[@class='py-2 px-3 border text-gray-600 rounded-md text-sm dark:text-gray-100 border-red-500 bg-red-500/10']")
            return ACCOUNT_RESTRICTED
        except Exception as e:
            pass
        sentence_responses.append(driver.find_elements(By.XPATH,"//div[@class='markdown prose w-full break-words dark:prose-invert light']")[index].text)
        time.sleep(3)
    return sentence_responses

def send_multiline_text(dialog_box,text:str,driver):
    subtexts=text.split('\n')
    for idx,subtext in enumerate(subtexts):
        dialog_box.send_keys(subtext)
        if idx != len(subtexts)-1:
            ActionChains(driver).key_down(Keys.SHIFT).key_down(Keys.ENTER).key_up(Keys.SHIFT).key_up(Keys.ENTER).perform()
            time.sleep(0.05)
    time.sleep(2)

def grab_elements_in_dialog_page(driver):
    send_dialog_button=WebDriverWait(driver,20,1).until(ec.presence_of_element_located((By.XPATH,"//button[@class='absolute p-1 rounded-md text-gray-500 bottom-1.5 md:bottom-2.5 hover:bg-gray-100 dark:hover:text-gray-400 dark:hover:bg-gray-900 disabled:hover:bg-transparent dark:disabled:hover:bg-transparent right-1 md:right-2']")))
    new_chat_button=WebDriverWait(driver,20,1).until(ec.presence_of_element_located((By.XPATH,"//a[@class='flex py-3 px-3 items-center gap-3 rounded-md hover:bg-gray-500/10 transition-colors duration-200 text-white cursor-pointer text-sm mb-2 flex-shrink-0 border border-white/20']")))
    dialog_box=WebDriverWait(driver,20,1).until(ec.presence_of_element_located((By.XPATH,"//textarea[@tabindex='0']")))
    if USE_GPT4:
        choose_button=WebDriverWait(driver,30,1).until(ec.presence_of_element_located((By.XPATH,"//button[@class='relative flex w-full cursor-default flex-col rounded-md border border-black/10 bg-white py-2 pl-3 pr-10 text-left focus:border-green-600 focus:outline-none focus:ring-1 focus:ring-green-600 dark:border-white/20 dark:bg-gray-800 sm:text-sm']")))
        choose_button.click()
        time.sleep(1)
        WebDriverWait(driver,30,1).until(ec.presence_of_element_located((By.XPATH,"//li[@class='text-gray-900 group relative flex h-[42px] cursor-pointer select-none items-center overflow-hidden border-b border-black/10 pl-3 pr-9 last:border-0 dark:border-white/20']")))
        choice_list=driver.find_elements(By.XPATH,"//li[@class='text-gray-900 group relative flex h-[42px] cursor-pointer select-none items-center overflow-hidden border-b border-black/10 pl-3 pr-9 last:border-0 dark:border-white/20']")
        #print(f'length of list: {len(choice_list)}')
        #for i in choice_list:print(i)
        choice_list[1].click()
    '''
        <li class="text-gray-900 group relative flex h-[42px] cursor-pointer select-none items-center overflow-hidden border-b border-black/10 pl-3 pr-9 last:border-0 dark:border-white/20" id="headlessui-listbox-option-:r1h:" role="option" tabindex="-1" aria-selected="true" data-headlessui-state="selected"><div class="flex items-center gap-1.5 truncate"><span class="font-semibold flex h-6 items-center gap-1 text-gray-800 dark:text-gray-100">GPT-4</span></div><span class="absolute inset-y-0 right-0 flex items-center pr-3 text-gray-800 dark:text-gray-100"><svg stroke="currentColor" fill="none" stroke-width="2" viewBox="0 0 24 24" stroke-linecap="round" stroke-linejoin="round" class="h-4 w-4 h-5 w-5" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><polyline points="20 6 9 17 4 12"></polyline></svg></span></li>

        <button class="relative flex w-full cursor-default flex-col rounded-md border border-black/10 bg-white py-2 pl-3 pr-10 text-left focus:border-green-600 focus:outline-none focus:ring-1 focus:ring-green-600 dark:border-white/20 dark:bg-gray-800 sm:text-sm" id="headlessui-listbox-button-:r0:" type="button" aria-haspopup="true" aria-expanded="false" data-headlessui-state="" aria-labelledby="headlessui-listbox-label-:r1: headlessui-listbox-button-:r0:"><label class="block text-xs text-gray-700 dark:text-gray-500" id="headlessui-listbox-label-:r1:" data-headlessui-state="">Model</label><span class="inline-flex w-full truncate"><span class="flex h-6 items-center gap-1 truncate">GPT-4</span></span><span class="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2"><svg stroke="currentColor" fill="none" stroke-width="2" viewBox="0 0 24 24" stroke-linecap="round" stroke-linejoin="round" class="h-4 w-4  text-gray-400" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><polyline points="6 9 12 15 18 9"></polyline></svg></span></button>'''
    return send_dialog_button,new_chat_button,dialog_box

def switch_to_newchat(driver,new_chat_button):
    new_chat_button.click()
    driver.implicitly_wait(10)
    time.sleep(3)

def get_sequential_dialog_response(driver,dialog) -> list[list[str]]:
    send_dialog_button,new_chat_button,dialog_box=grab_elements_in_dialog_page(driver)
    dialog_response=[]
    for index,sentence in enumerate(dialog):
        send_multiline_text(dialog_box,sentence,driver)
        send_dialog_button.click()
        response=get_sentence_result(driver,index)
        if response==ACCOUNT_RESTRICTED: return ACCOUNT_RESTRICTED
        dialog_response.append(response)
    switch_to_newchat(driver,new_chat_button)
    return dialog_response

def get_QA_dialog_response(driver,dialog) -> list[list[str]]:
    '''
        INPUT:
            input dialog is one answer to a certain unknown question 
            input=[original_answer]
        OUTPUT:
            output consists of two item, first item is the generated question corresponding to the original answer, second item is generated answers to the generated question, whose quantity is REGENERATE_COUNT  
            output=[[generated_question],[generated_ansewer0,generated_ansewer1,...]]
    '''
    send_dialog_button,new_chat_button,dialog_box=grab_elements_in_dialog_page(driver)
    dialog_response=[]
    original_answer=dialog[0]
    
    prompt_answer = "I want you to play the role of the questioner. I will type an answer in Chinese or English, and you will ask me a question based on the answer in the same language. Don't write any explanations or other text, just give me question."
    send_multiline_text(dialog_box,prompt_answer+'\n'+original_answer,driver)
    send_dialog_button.click()
    generated_question=get_sentence_result(driver,0,regenerate_time=0)
    if generated_question==ACCOUNT_RESTRICTED: return ACCOUNT_RESTRICTED
    dialog_response.append(generated_question)
    
    switch_to_newchat(driver,new_chat_button)

    send_dialog_button,new_chat_button,dialog_box=grab_elements_in_dialog_page(driver)
    send_multiline_text(dialog_box,generated_question[0],driver)
    send_dialog_button.click()
    generated_ansewers=get_sentence_result(driver,0)
    if generated_ansewers==ACCOUNT_RESTRICTED: return ACCOUNT_RESTRICTED
    dialog_response.append(generated_ansewers)

    switch_to_newchat(driver,new_chat_button)
    return dialog_response

def get_dialogs_response(driver,dialogs,spider_id):
    dialogs_response=[]
    for dialog in dialogs:
        dialog_response=get_sequential_dialog_response(driver,dialog)
        dialogs_response.append(dialog_response)
        logging.info(f'dialog {dialog} finished')
        with open(f'./output/result_{spider_id}.json','a') as f:
            f.write(json.dumps(dialog_response,ensure_ascii=False)+'\n')

def human_verify(driver):
    # human verification pecess
    logging.info('start verifacation')
    success=False
    enter_frame=False
    while not success:
        try:
            frame=driver.find_element(By.XPATH,"//iframe[@title='Widget containing a Cloudflare security challenge']")
            driver.switch_to.frame(frame)
            print('FRAME FOUND')
            enter_frame=True
            button=driver.find_element(By.XPATH,"//label[@class='ctp-checkbox-label']")
            logging.info('CLOUDFLARE VERIFACATION FOUND')
            button.click()
            success=True
            break
        except Exception as e:
            #print(e)
            if enter_frame:
                enter_frame=False
                driver.switch_to.default_content()
        try:
            button=driver.find_element(By.XPATH,"//input[@value='Verify you are human']")
            logging.info('OPENAI VERIFACATION FOUND')
            button.click()
            success=True
            break
        except Exception as e:
            #print(e)
            if enter_frame:
                enter_frame=False
                driver.switch_to.default_content()
        time.sleep(5)
    driver.implicitly_wait(10)
    time.sleep(10)
    login_button=driver.find_element(By.XPATH,"//button[@class='btn relative btn-primary']")

def login_in(driver,account,password):
    # login process
    login_button=WebDriverWait(driver,30,1).until(ec.presence_of_element_located((By.XPATH,"//button[@class='btn relative btn-primary']")))
    time.sleep(1)
    login_button.click()
    driver.implicitly_wait(30)
    time.sleep(3)
    print('login button pressed')
    try:
        #login_button=driver.find_element(By.XPATH,"//button[@class='btn flex justify-center gap-2 btn-primary']")
        #print('login in button find again')
        #login_button.click()
        driver.implicitly_wait(3)
        time.sleep(1)
        #print('login in button click again')
    except Exception as e:
        print('login button click only once')
        pass
    account_box=WebDriverWait(driver,30,1).until(ec.presence_of_element_located((By.XPATH,"//input[@id='username']")))
    time.sleep(1)
    account_box.send_keys(account)
    submit_button1=WebDriverWait(driver,30,1).until(ec.presence_of_element_located((By.XPATH,"//button[@type='submit']")))
    time.sleep(1)
    submit_button1.click()
    driver.implicitly_wait(30)
    time.sleep(2)
    print('continue button pressed')
    password_box=WebDriverWait(driver,30,1).until(ec.presence_of_element_located((By.XPATH,"//input[@id='password']")))
    time.sleep(1)
    password_box.send_keys(password)
    submit_button2=WebDriverWait(driver,30,1).until(ec.presence_of_element_located((By.XPATH,"//button[@type='submit']")))
    time.sleep(1)
    submit_button2.click()
    driver.implicitly_wait(30)
    print('continue2 button pressed')
    time.sleep(5)

def skip_guide(driver):
    next_button=WebDriverWait(driver,20,1).until(ec.presence_of_element_located((By.XPATH,"//button[@class='btn relative btn-neutral ml-auto']")))
    next_button.click()
    driver.implicitly_wait(10)
    print('guide click 1st')
    time.sleep(1)
    next_button=driver.find_element(By.XPATH,"//button[@class='btn relative btn-neutral ml-auto']")
    next_button.click()
    driver.implicitly_wait(10)
    print('guide click 2nd')
    time.sleep(1)
    next_button=driver.find_element(By.XPATH,"//button[@class='btn relative btn-primary ml-auto']")
    next_button.click()
    driver.implicitly_wait(10)
    print('guide click 3rd')
    time.sleep(3)

def err_callback(err):
    logging.error('subprocess error')
    logging.error(str(err))

def start_spider(account,password,multiprocess=False,spider_id=None,driver=None):
    # start logging  
    if not multiprocess:
        spider_id=time.strftime('%Y%m%d_%H%M%S',time.localtime())
        logging.basicConfig(filename=f'./logs/{spider_id}.log',level=logging.INFO,format=LOG_FORMAT,datefmt=DATE_FORMAT)
    logging.info(f'start single thread spider {spider_id}')
    
    if not multiprocess:
        options = Options()
        options.add_argument("--disable-popup-blocking")
        driver = uc.Chrome(options=options)
    driver.get('https://www.wikipedia.org/')
    driver.implicitly_wait(10)
    url='https://chat.openai.com/chat'
    while True:
        try:
            driver.get(url)
            driver.implicitly_wait(10)
            time.sleep(8)            
            try:
                login_button=driver.find_element(By.XPATH,"//button[@class='btn relative btn-primary']")
                logging.info('verifacation not found')
                break
            except Exception as e:
                logging.info('verifacation found')
                pass
            human_verify(driver)
            break
        except Exception as e:
            logging.error('verify failed, retrying')
            time.sleep(5)
    logging.info(f'{spider_id} verify success')        
    login_in(driver,account,password)
    logging.info(f'{spider_id} login success')
    
    try:
        skip_guide(driver)
        logging.info(f'{spider_id} guide detected and skipped')
    except Exception as e:
        logging.error(spider_id+':'+str(e))
        logging.info(f'{spider_id} guide undetected')
    return driver,spider_id
    

if __name__=='__main__':
    with open(INPUT_PATH,'r') as f:
        dialogs=json.load(f)
    id,password=random.sample(list(USER_LIST.items()),k=1)[0]
    driver,spider_id=start_spider(id,password)
    get_dialogs_response(driver,dialogs,spider_id)
    logging.info(f'{spider_id}: get dialogs end')