from concurrent.futures import ThreadPoolExecutor as pool
import threading as multi
from spider import start_spider,get_sequential_dialog_response,get_QA_dialog_response,ACCOUNT_RESTRICTED,ACCOUNT_USING
from config import *
import os 
import logging
import time
import json
import random
import traceback
from queue import Queue
from collections import OrderedDict
from selenium.webdriver.chrome.options import Options
import undetected_chromedriver as uc
import sys

if not os.path.exists('./logs'): os.mkdir('./logs')
cmd_output=sys.stdout
sys.stdout=open('./logs/debug.data','w',buffering=1)
sys.stderr=sys.stdout

spider_id=time.strftime('%Y%m%d_%H%M%S',time.localtime())
logging.basicConfig(filename=f'./logs/{spider_id}.log',level=logging.INFO,format=LOG_FORMAT,datefmt=DATE_FORMAT)
file_writing_lock=multi.Lock()
account_status_lock=multi.Lock()
unprocessed_dialog_queue=None
get_response=None
account_status=None #last time the account being restricted
account_is_using=None
valid_account_status_sorted=None # account ranked by last-restrict-time,ascending

def spider_concurrent(account,password,spider_id,outpath,hispath,driver):
    global valid_account_status_sorted
    global account_status_lock
    try:
        timestamp=time.time()
        account_status_lock.acquire()
        time_from_ban=timestamp-account_status.get(account,0)
        account_is_using[account]=True
        account_status_lock.release()
        if not time_from_ban>RESTRICT_TIME:
            waiting_time=RESTRICT_TIME-time_from_ban
            logging.info(f'{spider_id} waiting {waiting_time} seconds for account {account} to be unrestricted')
            time.sleep(waiting_time)
    except Exception as e:
        logging.error(f'{spider_id} account acquire failed')
        logging.error(str(e))
        logging.error(str(traceback.format_exc()))
        exit()
    try:
        driver,_=start_spider(account,password,True,spider_id,driver)
    except Exception as e:
        logging.error(f'{spider_id} set up failed')
        logging.error(str(e))
        logging.error(traceback.format_exc())
        exit()
    logging.info(f'{spider_id} Account {account} set up success ')
    while True:
        try:
            dialog_idx,dialog=unprocessed_dialog_queue.get(False)
        except Exception as e:
            logging.info('all input dialogs have been processed')
            sys.stdout=cmd_output
            print('all input dialogs have been processed, task completed')
            exit()
        t_pre=time.time()
        try:
            response=get_response(driver,dialog)
        except Exception as e:
            logging.error(f'{spider_id} encounters error when getting response')
            logging.error(str(e))
            logging.error(str(traceback.format_exc()))
            exit()
        t_after=time.time()
        if response==ACCOUNT_RESTRICTED:
            try:
                timestamp=time.time()
                driver.quit()
                logging.error(f'{spider_id} Account {account} is restricted')
                account_status_lock.acquire()
                if account_status_lock.locked(): print('is locked')
                else: print('not locked')
                account_status[account]=timestamp
                valid_account_status_sorted=sorted(valid_account_status_sorted,key=lambda k:account_status.get(k,0))
                with open('./logs/account.status','w') as f:
                    json.dump({account:time.strftime(DATE_FORMAT,time.localtime(t)) for account,t in account_status.items()},f)
                account_is_using[account]=False
                for new_account in valid_account_status_sorted:
                    if account_is_using[new_account] == True:
                        print(f'account {new_account} is using') 
                        continue
                    else: 
                        print(f'account {new_account} found, release')
                        time_from_ban=timestamp-account_status.get(new_account,0)
                        account_is_using[new_account]=True
                        account_status_lock.release()
                        break
                if not time_from_ban>RESTRICT_TIME:
                    waiting_time=RESTRICT_TIME-time_from_ban
                    logging.info(f'{spider_id} waiting {waiting_time} seconds for account {new_account} to be unrestricted')
                    time.sleep(waiting_time)
            except Exception as e:
                logging.error(f'{spider_id} encounters error when dealing with restricted account')
                logging.error(str(e))
                logging.error(str(traceback.format_exc()))
                exit()
            try:
                driver=uc.Chrome()
                driver,_=start_spider(new_account,USER_LIST[new_account],True,spider_id,driver)
                account=new_account
                continue
            except Exception as e:
                logging.error(f'{spider_id} switch to account {new_account} set up failed')
                logging.error(str(e))
                logging.error(str(traceback.format_exc()))
                exit()
        try:
            file_writing_lock.acquire()
            with open(outpath,'a') as f:
                f.write(json.dumps({'idx':dialog_idx,'response':response},ensure_ascii=False))
                f.write('\n')
            with open(hispath,'a') as f:
                f.write(str(dialog_idx)+' ')
            file_writing_lock.release()
            logging.info(f'spider {spider_id} get dialog{dialog_idx}\'s response, costs {int(t_after-t_pre)} seconds')
        except Exception as e:
            logging.error(f'{spider_id} encounters error when writing result')
            logging.error(str(e))
            logging.error(str(traceback.format_exc()))
            exit()

if __name__=='__main__':
    # mode setting
    if MODE=='Q&A': get_response=get_QA_dialog_response
    elif MODE=='sequential': get_response=get_sequential_dialog_response
    else:
        logging.error(f'unsupported MODE {MODE}')
        exit()

    # account setting
    if len(USER_LIST.keys()) < PROCESS_NUM:
        raise ValueError('process num must be smaller than user num')
    account_is_using={id:False for id in USER_LIST.keys()}    
    if not os.path.exists('./logs/account.status'): 
        with open ('./logs/account.status','w') as f:
            json.dump({},f)
        account_status={}
    else:
        with open ('./logs/account.status','r') as f:
            account_status={id:time.mktime(time.strptime(timestr,DATE_FORMAT)) for id,timestr in json.load(f).items()}
        logging.info(f'account status loaded')

    logging.info(f'start multi thread spider {spider_id}')
    with open(INPUT_PATH,'r') as f:
        dialogs=json.load(f)
        dialog_num=len(dialogs)
    taskname=INPUT_PATH.split('/')[-1].split('.')[0]
    out_file_path='./output/'+taskname+'.out'
    history_file_path='./output/'+taskname+'.history'
    
    if os.path.exists(history_file_path):
        logging.info(f'task {taskname} history found')
        with open(history_file_path,'r') as f:
            data=f.read()
        finished_dialog_idx=set([int(s) for s in data.split(' ')[:-1]])
    else:
        logging.info(f'task {taskname} history not found')
        finished_dialog_idx=set()
    
    unprocessed_dialog_queue=Queue(dialog_num)
    for dialog_idx,dialog in enumerate(dialogs):
        if not dialog_idx in finished_dialog_idx:
            unprocessed_dialog_queue.put_nowait([dialog_idx,dialog])
    
    valid_account_status_sorted=sorted(USER_LIST.keys(),key=lambda k:account_status.get(k,0))    
    accounts=[[valid_account_status_sorted[i],USER_LIST[valid_account_status_sorted[i]]] for i in range(PROCESS_NUM)]
    print(valid_account_status_sorted)

    drivers=[]
    for _ in range(PROCESS_NUM):
        options = Options()
        #options.add_argument('--headless')
        options.add_argument("--disable-popup-blocking")
        #options.add_argument('--start-maximized')
        #options.add_argument('--window-size=1280x1024')
        #options.add_argument('--blink-settings=imagesEnabled=false')
        #options.add_argument('--ignore-certificate-errors')
        drivers.append(uc.Chrome(options=options))
        time.sleep(2)
    
    idx=0
    p=pool(max_workers=PROCESS_NUM)
    for account,password in accounts:
        spider_id_subprocess=spider_id+'_'+str(idx).zfill(2)
        spider_concurrent(account,password,spider_id_subprocess,out_file_path,history_file_path,drivers[idx])
        #p.submit(spider_concurrent,account,password,spider_id_subprocess,out_file_path,history_file_path,drivers[idx])
        idx+=1
    
